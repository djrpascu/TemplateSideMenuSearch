angular.module('starter.controllers', [])

.controller('MenuController', function() {

})

.controller('HomeController', function() {

})

.controller('SearchController', function($scope, remoteService) {

  $scope.suggest = function(input) {
		if (input.length > 1) {
		remoteService.geocodeSuggest(input).then(
			function (result) {
				console.log(result);
				$scope.suggestions = result.data.suggestions;
			},
			function (error) {
				console.error(error);
			});
		}
		else {
			$scope.suggestions = null;
		}
	}

	$scope.search = function(input) {
		remoteService.geocodeFind(input).then(
			function (result) {
				console.log(result);

				$scope.suggestions = null;
				$scope.search.input = null;
			},
			function (error) {
				console.error(error);
			});
		}



})

.controller('BrowseController', function() {

})

.controller('MapController', function() {

});
