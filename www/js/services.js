angular.module('starter.services', [])

.factory('remoteService', function($http) {
	var service = {};
	var featureServiceUrl = "http://services6.arcgis.com/GppfEaYzw3YLUhtB/arcgis/rest/services/Hawaii_Public_Electric_Vehicle_Charging_Stations/FeatureServer/0";

	var geocodeServiceUrl = "http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer";
	var searchExtent =  "%7B%22xmin%22%3A-18024462.76586627%2C%22ymin%22%3A2103211.678467971%2C%22xmax%22%3A-17046068.80381606%2C%22ymax%22%3A2577121.25383604%2C%22spatialReference%22%3A%7B%22wkid%22%3A102100%7D%7D";

	service.getFeatures = function() {
		return $http({
				method: 'GET',
				url: featureServiceUrl + "/query?where=1%3D1&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&resultType=none&distance=&units=esriSRUnit_Meter&outFields=*&returnGeometry=true&multipatchOption=&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&returnDistinctValues=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&resultOffset=&resultRecordCount=&returnZ=false&returnM=false&quantizationParameters=&sqlFormat=none&f=pjson"
		});
	}

	service.geocodeSuggest = function(input) {
		return $http({
			method: 'GET',
			url: geocodeServiceUrl + "/suggest?text=" + input + "&searchExtent=" + searchExtent + "&f=json"
		});
	}

	service.geocodeFind = function(input) {
		return $http({
			method: 'GET',
			url: geocodeServiceUrl + "/findAddressCandidates?singleLine=" + input.text + "&magicKey=" + input.magicKey + "&f=json"
		});
	}

	return service;
});
